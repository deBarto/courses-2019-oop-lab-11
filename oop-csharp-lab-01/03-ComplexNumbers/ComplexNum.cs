﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                var mod = Math.Sqrt(Math.Pow(Re, 2) + Math.Pow(Im, 2));
                return mod;
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(Re, -Im);
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return (Re + ", " + Im);
        }
        
        public static ComplexNum operator+ (ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re + b.Re, a.Im + b.Im);
        }

        public static ComplexNum operator- (ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re - b.Re, a.Im - b.Im);
        }

        public static ComplexNum operator* (ComplexNum a, ComplexNum b)
        {
            return new ComplexNum((a.Re * b.Re) - (a.Im * b.Im), (a.Re * b.Im) + (a.Im * b.Re));
        }

        public static ComplexNum operator/ (ComplexNum a, ComplexNum b)
        {
            return new ComplexNum((a.Re * b.Re) - (a.Im * b.Im), (a.Re * b.Im) + (a.Im * b.Re));
        }
    }

}

    